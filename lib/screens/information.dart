import 'package:flutter/material.dart';
import 'package:minushia/model/data.dart';

class FAQPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.black),
        backgroundColor: Colors.white,
        title: Center(
            child: Text(
          'Information',
          style: TextStyle(fontSize: 24, color: Colors.grey),
        )),
      ),
      body: ListView.builder(
          itemCount: DataSource.questionAnswers.length,
          itemBuilder: (context, index) {
            return ExpansionTile(
              title: Row(
                children: <Widget>[
                  Icon(
                    Icons.do_not_disturb_on,
                    color: Colors.red,
                    size: 12,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Text(
                    DataSource.questionAnswers[index]['question'],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                        fontSize: 22),
                  ),
                ],
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(45.0, 12, 12, 12),
                  child: Text(
                    DataSource.questionAnswers[index]['answer'],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        // color: Colors.red,
                        fontSize: 16),
                  ),
                ),
              ],
            );
          }),
    );
  }
}
