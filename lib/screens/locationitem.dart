import 'package:flutter/material.dart';

class locationitem extends StatelessWidget {
  final String img;
  final String locationtype;
  final String area;

  const locationitem({Key key, this.img, this.locationtype, this.area})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  if (img != "")
                    Image(
                      image: NetworkImage(img),
                      height: 100,
                      width: 200,
                      fit: BoxFit.fitWidth,
                    ),
                ]),
          ),
          SizedBox(
            width: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      locationtype,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 22),
                    ),
                    Text(
                      area,
                      style: TextStyle(
                          //               // fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 16),
                    ),
                  ]),
            ),
          )
        ],
      ),
      decoration: new BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Colors.white),
    );
  }
}
