import 'package:flutter/material.dart';
import 'package:minushia/screens/home.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
      style: style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
        focusedBorder:
            UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
        enabledBorder:
            UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
      ),
    );

    final passwordField = TextField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
        border:
            UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
        disabledBorder:
            UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
        focusedBorder:
            UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
        enabledBorder:
            UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
      ),
    );

    final loginButon = Material(
      elevation: 5.0,
      color: Colors.red,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width / 2,
        padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => homepage()));
        },
        child: Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 145.0),
              Padding(
                padding: const EdgeInsets.only(right: 270),
                child: Text(
                  "Email",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
              ),
              emailField,
              SizedBox(height: 45.0),
              Padding(
                padding: const EdgeInsets.only(right: 230),
                child: Text(
                  "Password",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
              ),
              passwordField,
              SizedBox(
                height: 35.0,
              ),
              loginButon,
              SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
