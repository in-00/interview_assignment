import 'package:flutter/material.dart';
import 'package:minushia/model/data.dart';
import 'package:minushia/screens/details.dart';
import 'package:minushia/screens/information.dart';
import 'package:minushia/screens/locationitem.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class homepage extends StatelessWidget {
  const homepage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SlidingUpPanel(
            minHeight: 350.0,
            backdropEnabled: true,
            // borderRadius: radius,
            panel: Container(),
            collapsed: Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 12, right: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 2.0),
                    child: Text(
                      "FLUTTER TEST",
                      style: TextStyle(
                          fontSize: 28,
                          color: Colors.red,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 2.0),
                    child: Text(
                      "TESTING",
                      style: TextStyle(fontSize: 18, color: Colors.grey),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 2.0),
                    child: Text(
                      "Ramakrishnapura, Anekal Road, Chandapura P.O, Bengaluru, Karnataka 560099",
                      style: TextStyle(fontSize: 14, color: Colors.grey),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: DataSource.location.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            child: locationitem(
                                img: DataSource.location[index]["image"],
                                locationtype: DataSource.location[index]
                                    ["locationtype"],
                                area: DataSource.location[index]["area"]),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => detail(
                                          img: DataSource.location[index]
                                              ["image"],
                                          locationtype: DataSource
                                              .location[index]["locationtype"],
                                          area: DataSource.location[index]
                                              ["area"])));
                            },
                          );
                        }),
                  ),
                ],
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.only(bottom: 300),
              child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                      image: new DecorationImage(
                    image: NetworkImage(
                        'https://github.com/in-00/images/blob/master/map.jpg?raw=true'),
                  )),
                  child: new Stack(
                    children: <Widget>[
                      new Positioned(
                        right: 10.0,
                        top: 50.0,
                        child: new Icon(
                          Icons.arrow_drop_down_circle,
                          color: Colors.red,
                          size: 40,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FAQPage()));
                        },
                      ),
                    ],
                  )),
            )));
  }
}
