import 'package:flutter/material.dart';

class detail extends StatelessWidget {
  final String img;
  final String locationtype;
  final String area;

  const detail({Key key, this.img, this.locationtype, this.area})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      appBar: AppBar(
        leading: BackButton(color: Colors.black),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Container(
              width: 30,
              height: 25,
              child: Tab(
                icon: Image.network(
                    "https://github.com/in-00/images/blob/master/IMG_20200728_155857_328.jpg?raw=true"),
              ),
              decoration: BoxDecoration(
                color: Colors.red,
              ),
            ),
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 45,
          ),
          Image(
            image: NetworkImage(img),
            fit: BoxFit.fitWidth,
          ),
          Padding(
            padding: const EdgeInsets.all(32.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  locationtype,
                  style: TextStyle(
                      fontSize: 28,
                      color: Colors.red,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "Training",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(area,
                    style: TextStyle(
                      fontSize: 20,
                    ))
              ],
            ),
          )
        ],
      ),
    ));
  }
}
