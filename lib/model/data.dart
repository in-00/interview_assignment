import 'package:flutter/material.dart';

Color primaryBlack = Color(0xff202c3b);

class DataSource {
  static List questionAnswers = [
    {
      "question": "Orthomossaic",
      "answer":
          "Allow us to access the camera to take pictures and detect things"
    },
    {
      "question": "Digital Elevation",
      "answer":
          "Allow us to access the location to provide you location based features like tracking your activities more accurately and allowing you to tag location"
    },
    {
      "question": "Digital Terrain",
      "answer":
          "Allow us to access the location to provide you location based features like tracking your activities more accurately and allowing you to tag location"
    },
    {
      "question": "Vegetation",
      "answer":
          "Allow us to access the location to provide you location based features like tracking your activities more accurately and allowing you to tag location"
    },
  ];

  static List location = [
    {
      "locationtype": "Campus",
      "area": "142.88 Acre",
      "image": "https://github.com/in-00/images/blob/master/loc1.jpg?raw=true"
    },
    {
      "locationtype": "Farm",
      "area": "142.88 Acre",
      "image": "https://github.com/in-00/images/blob/master/loc2.jpg?raw=true"
    },
    // {
    //   "locationtype": "campus",
    //   "area": "142.88 Acre",
    //   "image": "https://github.com/in-00/images/blob/master/loc1.jpg?raw=true"
    // },
    // {
    //   "locationtype": "campus",
    //   "area": "142.88 Acre",
    //   "image": "https://github.com/in-00/images/blob/master/loc1.jpg?raw=true"
    // }
  ];
}
